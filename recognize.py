import sys
sys.path.append("../../")
import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst

from common.is_aarch_64 import is_aarch64
from common.bus_call import bus_call 
import pyds
import configparser
import ctypes
import numpy as np
import utils.face_utils as utils 

PGIE_CLASS_ID_VEHICLE = 0
PGIE_CLASS_ID_PERSON = 2

SGIE_CLASS_ID_LP = 1
SGIE_CLASS_ID_FACE = 0

pgie_classes = ["Vehicle", "TwoWheeler", "Person", "Roadsign"]

PRIMARY_DETECTOR_UID = 1
SECONDARY_DETECTOR_UID = 2

############################################################
# Changes need to made - SBK
import os
DATASET_PATH = "/media/bharath/New Volume1/Bharath@KI/scripts/OTF/embeddings_folder/embeddings_old.pkl"
training_file_update_trigger = "/media/bharath/New Volume1/Bharath@KI/scripts/OTF/training_data_trigger/update.txt"
face_embeddings, labels = utils.load_dataset(DATASET_PATH)
############################################################

def osd_sink_pad_buffer_probe(pad,info,u_data):
    global fps_stream, face_counter
    frame_number=0
    #Intiallizing object counter with 0.
    vehicle_count = 0
    person_count = 0
    face_count = 0
    lp_count = 0
    num_rects=0

    gst_buffer = info.get_buffer()
    if not gst_buffer:
        print("Unable to get GstBuffer ")
        return

    # Retrieve batch metadata from the gst_buffer
    # Note that pyds.gst_buffer_get_nvds_batch_meta() expects the
    # C address of gst_buffer as input, which is obtained with hash(gst_buffer)
    batch_meta = pyds.gst_buffer_get_nvds_batch_meta(hash(gst_buffer))
    l_frame = batch_meta.frame_meta_list
    while l_frame is not None:
        try:
            # Note that l_frame.data needs a cast to pyds.NvDsFrameMeta
            # The casting is done by pyds.glist_get_nvds_frame_meta()
            # The casting also keeps ownership of the underlying memory
            # in the C code, so the Python garbage collector will leave
            # it alone.
            frame_meta = pyds.NvDsFrameMeta.cast(l_frame.data)
        except StopIteration:
            break

        frame_number=frame_meta.frame_num
        num_rects = frame_meta.num_obj_meta
        l_obj=frame_meta.obj_meta_list
        while l_obj is not None:
            try:
                # Casting l_obj.data to pyds.NvDsObjectMeta
                obj_meta=pyds.NvDsObjectMeta.cast(l_obj.data)
            except StopIteration:
                break
            display_text = pyds.get_string(obj_meta.text_params.display_text)
            print("Display text inside primary: ",display_text)
            if obj_meta.unique_component_id == PRIMARY_DETECTOR_UID:
                if obj_meta.class_id == PGIE_CLASS_ID_VEHICLE:
                   vehicle_count += 1
                if obj_meta.class_id == PGIE_CLASS_ID_PERSON:
                   person_count += 1

            if obj_meta.unique_component_id == SECONDARY_DETECTOR_UID:
                if obj_meta.class_id == SGIE_CLASS_ID_FACE:
                   face_count += 1
                if obj_meta.class_id == SGIE_CLASS_ID_LP:
                   lp_count += 1
            
            try: 
                l_obj=l_obj.next
            except StopIteration:
                break
        #fps_stream.get_fps()
        # Acquiring a display meta object. The memory ownership remains in
        # the C code so downstream plugins can still access it. Otherwise
        # the garbage collector will claim it when this probe function exits.
        display_meta=pyds.nvds_acquire_display_meta_from_pool(batch_meta)
        display_meta.num_labels = 1
        py_nvosd_text_params = display_meta.text_params[0]
        #print("Text from display meta: ",pyds.get_string(py_nvosd_text_params.display_text))
        # Setting display text to be shown on screen
        # Note that the pyds module allocates a buffer for the string, and the
        # memory will not be claimed by the garbage collector.
        # Reading the display_text field here will return the C address of the
        # allocated string. Use pyds.get_string() to get the string content.
        py_nvosd_text_params.display_text = "Frame Number={} Number of Objects={}  Person_count={} Face Count={}".format(frame_number, num_rects, person_count, face_count)
        #face_counter.append(face_count)

        # Now set the offsets where the string should appear
        py_nvosd_text_params.x_offset = 10
        py_nvosd_text_params.y_offset = 12

        # Font , font-color and font-size
        py_nvosd_text_params.font_params.font_name = "Serif"
        py_nvosd_text_params.font_params.font_size = 10
        # set(red, green, blue, alpha); set to White
        py_nvosd_text_params.font_params.font_color.set(1.0, 1.0, 1.0, 1.0)

        # Text background color
        py_nvosd_text_params.set_bg_clr = 1
        # set(red, green, blue, alpha); set to Black
        py_nvosd_text_params.text_bg_clr.set(0.0, 0.0, 0.0, 1.0)
        # Using pyds.get_string() to get display_text as string
        #print(pyds.get_string(py_nvosd_text_params.display_text))
        pyds.nvds_add_display_meta_to_frame(frame_meta, display_meta)
        try:
            l_frame=l_frame.next
        except StopIteration:
            break
			
    return Gst.PadProbeReturn.OK

def sgie_sinkpad_buffer_probe(pad, info, u_data):
    global embed_array, pipeline, id_array
    frame_number = 0
    
    gst_buffer = info.get_buffer()
    if not gst_buffer:
        print("Unable to get buffer..")
        return 
    
    # get batch meta of gst_buffer
    batch_meta = pyds.gst_buffer_get_nvds_batch_meta(hash(gst_buffer))
    l_frame = batch_meta.frame_meta_list

    while l_frame is not None:
        try:
            frame_meta = pyds.NvDsFrameMeta.cast(l_frame.data)
        except StopIteration:
            break
        
        frame_number = frame_meta.frame_num
        l_obj = frame_meta.obj_meta_list

        while l_obj is not None:
            try:
                obj_meta = pyds.NvDsObjectMeta.cast(l_obj.data)
            except StopIteration:
                break

            l_user = obj_meta.obj_user_meta_list

            while l_user is not None:
                try:
                    user_meta = pyds.NvDsUserMeta.cast(l_user.data)
                except StopIteration:
                    break
                
                if user_meta.base_meta.meta_type != pyds.NvDsMetaType.NVDSINFER_TENSOR_OUTPUT_META:
                    continue

                tensor_meta = pyds.NvDsInferTensorMeta.cast(user_meta.user_meta_data)
                layer = pyds.get_nvds_LayerInfo(tensor_meta, 0)

                ptr = ctypes.cast(pyds.get_ptr(layer.buffer), ctypes.POINTER(ctypes.c_float))
                v = np.ctypeslib.as_array(ptr, shape=(128,))
                
                # predict the class
                yhat = v.reshape((1,-1))
                normalized_embed = utils.normalize_vectors(yhat)
                
                ############################## SBK ##########################################
                if os.path.exists(training_file_update_trigger):
                    face_embeddings, labels = utils.load_dataset(DATASET_PATH)
                    os.unlink(training_file_update_trigger)
                #############################################################################
                result, prob = utils.recognize_face(face_embeddings, labels, normalized_embed)
                result = str(result).title()
                print(f"Recognized person: {result} with confidence {prob}")

                classifier_meta = pyds.nvds_acquire_classifier_meta_from_pool(batch_meta)
                classifier_meta.unique_component_id = tensor_meta.unique_id

                label_info = pyds.nvds_acquire_label_info_meta_from_pool(batch_meta)
                label_info.result_prob = 0
                label_info.result_class_id = 0

                pyds.nvds_add_label_info_meta_to_classifier(classifier_meta, label_info)
                pyds.nvds_add_classifier_meta_to_object(obj_meta, classifier_meta)

                object_meta = pyds.nvds_acquire_obj_meta_from_pool(batch_meta)
                #fram_meta = pyds.nvds_acquire_frame_meta_from_pool(batch_meta)

                display_text = pyds.get_string(obj_meta.text_params.display_text)
                #print("Display text: ",display_text)
                #obj_meta.text_params.display_text = result
                obj_meta.text_params.display_text = result
                new_display_text = pyds.get_string(obj_meta.text_params.display_text)
                #print("New display text: ",new_display_text)
                
                #pyds.nvds_add_obj_meta_to_frame(frame_meta, obj_meta, None)

                #print(type(fram_meta), type(object_meta))
                #pyds.nvds_add_obj_meta_to_frame(fram_meta, object_meta)
                #pyds.nvds_add_frame_meta_to_batch(batch_meta, frame_meta)

                try:
                    l_user = l_user.next
                except StopIteration:
                    break

            try:
                l_obj = l_obj.next
            except StopIteration:
                break

        try:
            l_frame = l_frame.next
        except StopIteration:
            break
    
    return Gst.PadProbeReturn.OK

def main(args):
    if len(args) < 2:
        sys.stderr.write("usage: %s <v4l2-device-path>\n" %(args[0]))
        sys.exit(1)

    # G library inits
    GObject.threads_init()
    Gst.init(None)

    # create Gstreamer/Deepstream elements to form a pipeline
    ## init a pipeline first
    print("Creating Pipeline\n")
    pipeline = Gst.Pipeline()
    if not pipeline:
        sys.stderr.write("Unable to create a pipeline")

    # source element for reading from file
    print("Creating Source element\n")
    source = Gst.ElementFactory.make('v4l2src', 'usb-cam-source')
    if not source:
        sys.stderr.write("Unable to create source")

    # data format conversion 
    print("Creating Capsfilter 1 \n")
    caps_src = Gst.ElementFactory.make('capsfilter', 'v4l2src_caps')
    if not caps_src:
        sys.stderr.write("Unable to create capfilter 1")

    # convert from RAW to supported format
    print("Creating video converter 1\n")
    vidconv = Gst.ElementFactory.make('videoconvert', 'converter-src1')
    if not vidconv:
        sys.stderr.write("Unable to create video converter 1")

    # convert from supported format to NVMM Mem
    print("Creating video converter 2\n")
    nvvidconv = Gst.ElementFactory.make('nvvideoconvert', 'converter-src2')
    if not nvvidconv:
        sys.stderr.write("Unable to create video converter 2")
    
    # data format conversion 
    print("Creating Capsfilter 2 \n")
    caps_nvmm = Gst.ElementFactory.make('capsfilter', 'nvmm_caps')
    if not caps_nvmm:
        sys.stderr.write("Unable to create capfilter 2")

    # Create nvstreammux instance to form batches from one or more sources.
    streammux = Gst.ElementFactory.make("nvstreammux", "Stream-muxer")
    if not streammux:
        sys.stderr.write(" Unable to create NvStreamMux \n")
    
    # primary inference element
    pgie = Gst.ElementFactory.make('nvinfer', 'primary-inference')
    if not pgie:
        sys.stderr.write(" Unable to create inference element \n")

    # tracker element
    tracker = Gst.ElementFactory.make('nvtracker', 'tracker')
    if not tracker:
        sys.stderr.write(" Unable to create tracker element \n")

    sgie1 = Gst.ElementFactory.make("nvinfer", "secondary1-nvinference-engine")
    if not sgie1:
        sys.stderr.write(" Unable to make sgie1 \n")

    # convert from nv12 to rgba for nvosd
    nvconv = Gst.ElementFactory.make('nvvideoconvert', 'convertor')
    if not nvconv:
        sys.stderr.write("Unable to create convertor\n")
    
    # create osd element to draw on the rgba buffer
    nvosd = Gst.ElementFactory.make('nvdsosd', 'onscreendisplay')
    if not nvosd:
        sys.stderr.write(" Unable to create osd element \n")

    if is_aarch64():
        transform = Gst.ElementFactory.make('nvegltransform', 'nvegl-transform')
    
    # create egl sink
    print("Creating EGL sink element")
    sink = Gst.ElementFactory.make('nveglglessink', 'nvvideo-renderer')
    if not sink:
        sys.stderr.write(" Unable to create video renderer element \n")

    # set properties
    print("Playing cam %s " %args[1])
    source.set_property('device', args[1])
    caps_src.set_property('caps', Gst.Caps.from_string('video/x-raw, framerate=30/1'))
    caps_nvmm.set_property('caps', Gst.Caps.from_string('video/x-raw(memory:NVMM)'))
    streammux.set_property('width', 1920)
    streammux.set_property('height', 1080)
    streammux.set_property('batch-size', 1)
    streammux.set_property('batched-push-timeout', 4000000)
    pgie.set_property('config-file-path', "../configs/face_pgie_config.txt")
    tracker.set_property('display-tracking-id', 0)
    sgie1.set_property('config-file-path', "../configs/sgie_config.txt")
    sink.set_property('sync', False)
    

    # set properties of tracker
    config = configparser.ConfigParser()
    config.read('../configs/face_tracker_config.txt')
    config.sections()

    for key in config['tracker']:
        if key == 'tracker-width':
            tracker_width = config.getint('tracker', key)
            tracker.set_property('tracker-width', tracker_width)
        if key == 'tracker-height':
            tracker_height = config.getint('tracker', key)
            tracker.set_property('tracker-height', tracker_height)
        if key == 'gpu-id':
            gpu_id = config.getint('tracker', key)
            tracker.set_property('gpu-id', gpu_id)
        if key == 'll-lib-file':
            ll_lib_file = config.get('tracker', key)
            tracker.set_property('ll-lib-file', ll_lib_file)
        if key == 'll-config-file':
            ll_config_file = config.get('tracker', key)
            tracker.set_property('ll-config-file', ll_config_file)
        if key == 'enable-batch-process':
            enable_batch_process = config.getint('tracker', key)
            tracker.set_property('enable-batch-process', enable_batch_process)
        if key == 'enable-past-frame':
            enable_past_frame = config.getint('tracker', key)
            tracker.set_property('enable-past-frame', enable_past_frame)

    print("Adding elements to Pipeline \n")
    pipeline.add(source)
    pipeline.add(caps_src)
    pipeline.add(vidconv)
    pipeline.add(nvvidconv)
    pipeline.add(caps_nvmm)
    pipeline.add(streammux)
    pipeline.add(pgie)
    pipeline.add(tracker)
    pipeline.add(sgie1)
    pipeline.add(nvconv)
    pipeline.add(nvosd)
    pipeline.add(sink)
    if is_aarch64():
        pipeline.add(transform)

    # now link the elements
    # source -> decoder -> muxer -> infer -> convertor -> osd -> transform -> renderer
    print("Linking elements in the Pipeline \n")
    source.link(caps_src)
    caps_src.link(vidconv)
    vidconv.link(nvvidconv)
    nvvidconv.link(caps_nvmm)
    
    sinkpad = streammux.get_request_pad("sink_0")
    if not sinkpad:
        sys.stderr.write(" Unable to get the sink pad of streammux \n")
    srcpad = caps_nvmm.get_static_pad("src")
    if not srcpad:
        sys.stderr.write(" Unable to get source pad of vidconv capsfilter  \n")
    srcpad.link(sinkpad)
    streammux.link(pgie)
    pgie.link(tracker)
    tracker.link(sgie1)
    sgie1.link(nvconv)
    nvconv.link(nvosd)
    if is_aarch64():
        nvosd.link(transform)
        transform.link(sink)
    else:
        nvosd.link(sink)

    # create an event loop and feed gstreamer bus mesages to it
    loop = GObject.MainLoop()
    bus = pipeline.get_bus()
    bus.add_signal_watch()
    bus.connect ("message", bus_call, loop)

    nvosdsinkpad = nvosd.get_static_pad('sink')
    if not nvosdsinkpad:
        sys.stderr.write("Unable to get sink pad of nvosd")
    nvosdsinkpad.add_probe(Gst.PadProbeType.BUFFER, osd_sink_pad_buffer_probe, 0)


    nvconvsinkpad = nvconv.get_static_pad('sink')
    if not nvconvsinkpad:
        sys.stderr.write('Unable to get sink pad of nvconv')
    nvconvsinkpad.add_probe(Gst.PadProbeType.BUFFER, sgie_sinkpad_buffer_probe, 0)

    # start play back and listen to events
    print("Starting pipeline \n")
    pipeline.set_state(Gst.State.PLAYING)
    try:
        loop.run()
    except:
        pass
    # cleanup
    pipeline.set_state(Gst.State.NULL)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
