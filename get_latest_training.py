###############################################################
# Developer : S Bharath Kumar
# Created on : 21/03/2022
# Description : This scipt will append the delta (training data)
#               to the training file.
###############################################################
import pickle
from sklearn.preprocessing import Normalizer
import os
import glob
from datetime import datetime
import numpy as np
import pysftp

# Training pickle file folder path and trigger file path
training_data_pickle_file_path = "/media/bharath/New Volume1/Bharath@KI/scripts/OTF/embeddings_folder/embeddings_old.pkl"
training_data_pickle_file_save_path = "/media/bharath/New Volume1/Bharath@KI/scripts/OTF/embeddings_folder/embeddings.pkl"
training_data_update_trigger_file_path = "/media/bharath/New Volume1/Bharath@KI/scripts/OTF/training_data_trigger/update.txt"

# SFTP variables
myHostname = "tvsthalli.com"
myUsername = "root"
myPassword = "12345"
remoteFilePath = "/home/fr/embeddings.pkl"
training_data_updtae_trigger_file_path_remote = "/media/bharath/New Volume1/Bharath@KI/scripts/OTF/training_data_trigger/update.txt"


# Get the latest training file
def get_latest_file(list_of_files):
    latest_file = max(list_of_files, key=os.path.getctime)
    return latest_file

# Delete old training file
def delete_old_training_file(file_name):
    os.unlink(file_name)

# Connect to the remote server and pull the latest training file.
def fetch_latest_training_file(myHostname,myUsername,myPassword,remoteFilePath,local_path_to_training_file,training_data_updtae_trigger_file_path_remote):
    try:
        with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword) as sftp:
            print("Connection established successfully")
            if sftp.exists(training_data_updtae_trigger_file_path_remote):
                # Get the file from the remote server.            
                sftp.get(remoteFilePath ,local_path_to_training_file) 
                return "Success" 
            else:
                print("No latest file is available")
                return ""
    except:
        print("Process is failed, please try once again")
    
if __name__ =="__main__":
    try:
        # Pull the latest training file
        latest_training_update_status = fetch_latest_training_file(myHostname,myUsername,myPassword,remoteFilePath,training_data_pickle_file_path)

        if latest_training_update_status == "Success":
            # clear old training file
            delete_old_training_file(training_data_pickle_file_path)
            os.rename(training_data_pickle_file_save_path,training_data_pickle_file_path)
            # Create a trigger file to reintialize the embeddings 
            if not os.path.exists(training_data_update_trigger_file_path):
                open(training_data_update_trigger_file_path, 'a').close()
    except:
        print("Process is failed, please try once again")
        